<?php
require 'database.php';
header("Content-Type: application/json");
session_start();

if(isset($_SESSION['user_id'])) {
	$username = trim($_SESSION['user_id']);
	$id = $_POST['id'];

	$stmt = $mysqli->prepare("DELETE from events where id = ? and username = ?");
	if(!$stmt){
	 echo json_encode(array(
		"success" => false,
		"message" => "Event Delete Failure"
	));

	exit;
	}

	$stmt->bind_param('ss', $id, $username);
	 
	$stmt->execute();
	 
	$stmt->close();

	 echo json_encode(array(
		"success" => true,
		"message" => "Congrats, you deleted an event"
	));
	exit;

}
?>