<?php
    session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Calendar</title>
        <link rel="stylesheet" type="text/css" href="calendar.css">
	</head>

    <body>
        <h1 id="monthname"></h1>
        <div id="cal">
            <p id="usernameSecret" hidden></p>
   
            <table id="calendartable">
                <tr>
                    <th>Sunday</th>
                    <th>Monday</th>
                    <th>Tuesday</th>
                    <th>Wednesday</th>
                    <th>Thursday</th>
                    <th>Friday</th>
                    <th>Saturday</th>
                </tr>
                <tr class="week" id="week0"></tr>
                <tr class="week" id="week1"></tr>
                <tr class="week" id="week2"></tr>
                <tr class="week" id="week3"></tr>
                <tr class="week" id="week4"></tr>
            </table>
            <button id="previousmonth_btn">Previous Month</button>
            <button id="nextmonth_btn">Next Month</button>
        </div>
        <div id="login">
                <h4>LOGIN</h4>
                <label for='username' >UserName:</label>
                <input type='text' name='username' id='username'  maxlength="50" />
                <br>
                <label for='password' >Password:</label>
                <input type='password' name='password' id='password'  maxlength="100" />
                <br>
                <button id="login_btn">Login</button>
        </div>

        <div id="signup">
                <h4>SIGN UP</h4>
                <label for='username' >UserName:</label>
                <input type='text' name='username' id='username1'  maxlength="50" />
                <br>
                <label for='password' >Password:</label>
                <input type='password' name='password' id='password1'  maxlength="100" />
                <br>
                <button id="signup_btn">Signup</button>
        </div>

        <div id="addevent">
                <h4>ADD EVENT</h4>
                <label for='date' >Date:</label>
                <input type='text' placeholder='YYYY-MM-DD' id='date' />
                <br>
                <label for='time' >Time:</label>
                <input type='time' name='time' id='time' />
                <br>
                <label for='event' >Event Name:</label>
                <input type='text' name='event' id='event' />
                <br>
                <button id="addevent_btn">Add Event</button>
        </div>

                <div id="editdeleteevent">
                <h4>EDIT/DELETE EVENT</h4>
                <label for='id' >ID:</label>
                <input type='text' id='id1' />
                <br>
                <label for='date' >Date:</label>
                <input type='text' placeholder='YYYY-MM-DD' id='date1' />
                <br>
                <label for='time' >Time:</label>
                <input type='time' name='time' id='time1' />
                <br>
                <label for='event' >Event Name:</label>
                <input type='text' name='event' id='event1' />
                <br>
                <button id="editevent_btn">Edit Event</button>
                <button id="deleteevent_btn">Delete Event</button>
        </div>

        <script type="text/javascript" src="calendar.js"></script>
        <script type="text/javascript" src="ajax.js"></script>
    </body>

</html>