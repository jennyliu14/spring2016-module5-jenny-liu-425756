var currentDate = new Date();

var currentmonth = new Month(currentDate.getFullYear(), currentDate.getMonth());

var operation = document.addEventListener("DOMContentLoaded", updatecalendar, false);

function signup(event){
	var username = document.getElementById("username1").value; // Get the username from the form
	var password = document.getElementById("password1").value; // Get the password from the form
 
	// Make a URL-encoded string for passing POST data:
	var dataString = "username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password);
 
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "adduser.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
		}else{
			alert("You were not logged in.  "+jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
	updatecalendar();
}

function loginAjax(event){
	var username = document.getElementById("username").value; // Get the username from the form
	var password = document.getElementById("password").value; // Get the password from the form
 
	// Make a URL-encoded string for passing POST data:
	var dataString = "username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password);
 
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "login.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			alert("You've been Logged In!");
			document.getElementById("usernameSecret").innerHTML = username;
			updatecalendar();
			loadevents();
		}else{
			alert("You were not logged in.  "+jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data

}

function addeventAjax(event){
	var username = document.getElementById("usernameSecret").innerHTML; // Get the username from the form
	var date = document.getElementById("date").value;
	var time = document.getElementById("time").value;
	var calevent = document.getElementById("event").value;
 
	// Make a URL-encoded string for passing POST data:
	var dataString = "&date=" + encodeURIComponent(date) + "&time=" + encodeURIComponent(time) + "&calevent=" + encodeURIComponent(calevent);
 
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "add_event.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData

		}else{
			alert("You were not logged in.  "+jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
	loadevents();
}

function editeventAjax(event){
	var username = document.getElementById("usernameSecret").innerHTML; // Get the username from the form
	var date = document.getElementById("date1").value;
	var time = document.getElementById("time1").value;
	var calevent = document.getElementById("event1").value;
	var id = document.getElementById("id1").value;
 
	// Make a URL-encoded string for passing POST data:
	var dataString = "&date=" + encodeURIComponent(date) + "&time=" + encodeURIComponent(time) + "&calevent=" + encodeURIComponent(calevent) + "&id=" + encodeURIComponent(id);
 
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "edit_event.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			loadevents();
		}else{
			alert("You were not logged in.  "+jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
}

function deleteeventAjax(event){
	var username = document.getElementById("usernameSecret").innerHTML; // Get the username from the form
	var date = document.getElementById("date1").value;
	var time = document.getElementById("time1").value;
	var calevent = document.getElementById("event1").value;
	var id = document.getElementById("id1").value;
 
	// Make a URL-encoded string for passing POST data:
	var dataString = "&date=" + encodeURIComponent(date) + "&time=" + encodeURIComponent(time) + "&calevent=" + encodeURIComponent(calevent) + "&id=" + encodeURIComponent(id);
 
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "delete_event.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			loadevents();
		}else{
			alert("You were not logged in.  "+jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
}

function loadevents(event){
	var username = document.getElementById("usernameSecret").value; // Get the username from the form
 
	// Make a URL-encoded string for passing POST data:
	var dataString = "username=" + encodeURIComponent(username);
 
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "loadevents.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var events = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		// in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
		updatecalendar();
		for (var e in events) {
			var EventId = events[e].EventId;
			var EventName = events[e].EventName;
			var EventDate = new Date(events[e].EventDate);
			var EventTime = events[e].EventTime;

			var formattedEventDate = "" + EventDate.getFullYear()+ "-" + (EventDate.getMonth() + 1) + "-" + (EventDate.getDate() + 1);
		
		if (EventDate.getMonth() == currentmonth.month) {
			var newlist = document.createElement("li");
			newlist.appendChild(document.createTextNode("ID:" + EventId + " " + EventName + "\n" + " at " + EventTime));
			document.getElementById(formattedEventDate).appendChild(newlist);	
		}
		}
		
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
}
var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

function updatecalendar(){
	document.getElementById("monthname").innerHTML = months[currentmonth.month];
	for (var i=0; i<5; i++) {
		document.getElementById("week"+i).innerHTML = " ";
	}
	var weeks;
	weeks = currentmonth.getWeeks();
	for(var w in weeks){ //for each week, w, in our weeks array
		var days = weeks[w].getDates(); //get the days for week w and store it in an array called days
		for(var d in days){ //for each day, d, in days array
			var day = days[d]; //day is the current day we are looking at in the for loop execution cycle. day is the current date we are trying to make a cell for in our html calendar
			var dateID = "" + day.getFullYear() + "-" + (day.getMonth() + 1) + "-" + day.getDate();
			document.getElementById("week"+w).innerHTML += "<td class='day'><p>" + day.getDate() + "</p><ol id=\"" + dateID + "\"></ol></td>";

		}
	}


}

function previousmonth(){
	currentmonth = currentmonth.prevMonth();
	loadevents();
}

function nextmonth(){
	currentmonth = currentmonth.nextMonth();
	loadevents();
}

document.getElementById("signup_btn").addEventListener("click", signup, false); // Bind the AJAX call to button click
document.getElementById("login_btn").addEventListener("click", loginAjax, false); // Bind the AJAX call to button click
document.getElementById("addevent_btn").addEventListener("click", addeventAjax, false); // Bind the AJAX call to button click
document.getElementById("previousmonth_btn").addEventListener("click", previousmonth, false); // Bind the AJAX call to button click
document.getElementById("nextmonth_btn").addEventListener("click", nextmonth, false); // Bind the AJAX call to button click
document.getElementById("editevent_btn").addEventListener("click", editeventAjax, false); // Bind the AJAX call to button click
document.getElementById("deleteevent_btn").addEventListener("click", deleteeventAjax, false); // Bind the AJAX call to button click
