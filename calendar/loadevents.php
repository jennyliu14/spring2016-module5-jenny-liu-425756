<?php
require 'database.php';
header("Content-Type: application/json");
session_start();

$user = $_SESSION['user_id'];

// Use a prepared statement
$stmt = "SELECT * FROM events WHERE username='".$user."'";
 
$events = array();
$result = mysqli_query($mysqli, $stmt);

while($row = mysqli_fetch_array($result)){

	$row_array['EventId'] = htmlentities($row['id']);
	$row_array['EventName'] = htmlentities($row['event']);
	$row_array['EventDate'] = htmlentities($row['date']);
	$row_array['EventTime'] = htmlentities($row['time']);
	array_push($events, $row_array);
}

mysqli_close($mysqli);

echo json_encode($events);

exit;

?>