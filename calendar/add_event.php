<?php
require 'database.php';
header("Content-Type: application/json");
session_start();


if(isset($_SESSION['user_id'])) {
	$username = trim($_SESSION['user_id']);
	$date = $_POST['date'];
	$event = $_POST['calevent'];
	$time = $_POST['time'];

	$stmt = $mysqli->prepare("insert into events (username, date, time, event) values (?, ?, ?, ?)");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}

	$stmt->bind_param('ssss', $username, $date, $time, $event);
	 
	$stmt->execute();
	 
	$stmt->close();

	 echo json_encode(array(
		"success" => true,
		"message" => "Congrats, you made an event"
	));
	exit;

}

 echo json_encode(array(
		"success" => false,
		"message" => "Event Add Failure"
	));

exit;
?>