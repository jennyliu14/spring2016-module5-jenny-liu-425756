<?php
require 'database.php';
header("Content-Type: application/json");
session_start();


if(isset($_SESSION['user_id'])) {
	$username = trim($_SESSION['user_id']);
	$date = $_POST['date'];
	$event = $_POST['calevent'];
	$time = $_POST['time'];
	$id = $_POST['id'];

	$stmt = $mysqli->prepare("UPDATE events set date = ?, event = ?, time = ? where id = ? and username = ?");
	if(!$stmt){
	 echo json_encode(array(
		"success" => false,
		"message" => "Event Add Failure"
	));

	exit;
	}

	$stmt->bind_param('sssss', $date, $event, $time, $id, $username);
	 
	$stmt->execute();
	 
	$stmt->close();

	 echo json_encode(array(
		"success" => true,
		"message" => "Congrats, you edited an event"
	));
	exit;

}


?>